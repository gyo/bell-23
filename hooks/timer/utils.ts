import { DateTime } from "luxon";

import { Time, Times } from "../../types/timer";

export const refreshPlayedDate = (
  time: Time,
  currentTime: Time,
  currentDate: number
) => {
  let playedDate: Time["playedDate"];
  if (time.hour < currentTime.hour) {
    playedDate = currentDate;
  } else if (time.hour > currentTime.hour) {
    playedDate = undefined;
  } else if (time.minute < currentTime.minute) {
    playedDate = currentDate;
  } else if (time.minute > currentTime.minute) {
    playedDate = undefined;
  } else {
    playedDate = currentDate;
  }
  return {
    ...time,
    playedDate,
  };
};

export const refreshPlayedDates = (times: Times) => {
  const now = DateTime.local();
  const day = now.day;
  const hour = now.hour;
  const minute = now.minute;

  return resetPlayedDates(times, { hour, minute }, day);
};

export const resetPlayedDates = (
  times: Times,
  currentTime: Time,
  currentDate: number
) => {
  return times.map((time) => {
    return refreshPlayedDate(time, currentTime, currentDate);
  });
};

export const isTimeForPlay = (
  time: Time,
  day: number,
  hour: number,
  minute: number
) => {
  if (time.hour !== hour || time.minute !== minute) {
    // 時刻が一致していなければ鳴らさない
    return false;
  } else if (time.playedDate === day) {
    // 時刻が一致していても、day が一致していれば鳴らさない
    return false;
  } else {
    // 時刻が一致していて、day が一致していなければ鳴らす
    return true;
  }
};

export const playAudio = (audio: HTMLAudioElement | null, muted: boolean) => {
  if (audio == null) {
    return;
  }
  audio.currentTime = 0;
  if (muted) {
    return;
  }
  audio.play();
};

export const notify = (hour: number, minute: number) => {
  if (window.Notification == null) {
    return;
  }
  new window.Notification(
    `${hour}`.padStart(2, "0") + ":" + `${minute}`.padStart(2, "0"),
    {
      icon: "logo192.png",
    }
  );
};
