import { isTimeForPlay, refreshPlayedDate, resetPlayedDates } from "./utils";

describe("refreshPlayedDate", () => {
  test("played because hour", () => {
    expect(
      refreshPlayedDate({ hour: 1, minute: 2 }, { hour: 2, minute: 2 }, 1)
    ).toStrictEqual({ hour: 1, minute: 2, playedDate: 1 });
  });
  test("played because minute", () => {
    expect(
      refreshPlayedDate({ hour: 2, minute: 1 }, { hour: 2, minute: 2 }, 1)
    ).toStrictEqual({ hour: 2, minute: 1, playedDate: 1 });
  });
  test("not played because minute", () => {
    expect(
      refreshPlayedDate({ hour: 2, minute: 2 }, { hour: 2, minute: 2 }, 1)
    ).toStrictEqual({ hour: 2, minute: 2, playedDate: 1 });
  });
  test("not played because minute", () => {
    expect(
      refreshPlayedDate({ hour: 2, minute: 3 }, { hour: 2, minute: 2 }, 1)
    ).toStrictEqual({ hour: 2, minute: 3, playedDate: undefined });
  });
  test("not played because hour", () => {
    expect(
      refreshPlayedDate({ hour: 3, minute: 2 }, { hour: 2, minute: 2 }, 1)
    ).toStrictEqual({ hour: 3, minute: 2, playedDate: undefined });
  });
});

test("resetPlayedDates", () => {
  expect(
    resetPlayedDates(
      [
        { hour: 1, minute: 2 },
        { hour: 2, minute: 1 },
        { hour: 2, minute: 2 },
        { hour: 2, minute: 3 },
        { hour: 3, minute: 2 },
      ],
      { hour: 2, minute: 2 },
      1
    )
  ).toStrictEqual([
    { hour: 1, minute: 2, playedDate: 1 },
    { hour: 2, minute: 1, playedDate: 1 },
    { hour: 2, minute: 2, playedDate: 1 },
    { hour: 2, minute: 3, playedDate: undefined },
    { hour: 3, minute: 2, playedDate: undefined },
  ]);
});

describe("isTimeForPlay", () => {
  test("true", () => {
    expect(isTimeForPlay({ hour: 1, minute: 1, playedDate: 1 }, 31, 1, 1)).toBe(
      true
    );
  });

  test("false because hour", () => {
    expect(
      isTimeForPlay({ hour: 1, minute: 1, playedDate: 1 }, 31, 12, 1)
    ).toBe(false);
  });

  test("false because minute", () => {
    expect(
      isTimeForPlay({ hour: 1, minute: 1, playedDate: 1 }, 31, 1, 59)
    ).toBe(false);
  });

  test("false because playedDate", () => {
    expect(isTimeForPlay({ hour: 1, minute: 1, playedDate: 1 }, 1, 1, 1)).toBe(
      false
    );
  });
});
