import { DateTime } from "luxon";
import { useCallback, useEffect, useRef, useState } from "react";

import { Times } from "../../types/timer";
import { isTimeForPlay, notify, playAudio, refreshPlayedDates } from "./utils";

export const useTimer = (
  timerId: string,
  defaultTimes?: Times,
  onPlay?: () => void,
  onEnd?: () => void
) => {
  const audioElementRef = useRef<HTMLAudioElement | null>(null);

  const [times, setTimes] = useState<Times>([]);
  const [muted, setMuted] = useState(false);
  const [volume, setVolume] = useState<number | "">(100);

  const updateTimes = useCallback(
    (nextTimes: Times) => {
      const refreshedTimes = refreshPlayedDates(nextTimes);
      setTimes(refreshedTimes);
      localStorage.setItem(timerId, JSON.stringify(refreshedTimes));
    },
    [timerId]
  );

  const updateMuted = useCallback(
    (nextMuted: boolean) => {
      setMuted(nextMuted);
      localStorage.setItem(`${timerId}-muted`, JSON.stringify(nextMuted));
    },
    [timerId]
  );

  const updateVolume = useCallback(
    (nextVolume: number | "") => {
      setVolume(nextVolume);
      localStorage.setItem(`${timerId}-volume`, JSON.stringify(nextVolume));
    },
    [timerId]
  );

  // times 初期化
  useEffect(() => {
    let times: Times = [];
    const storageData = localStorage.getItem(timerId);
    if (storageData == null && defaultTimes != null) {
      times = defaultTimes;
    }
    if (storageData != null) {
      times = JSON.parse(storageData);
    }
    updateTimes(times);
  }, [defaultTimes, timerId, updateTimes]);

  // muted 初期化
  useEffect(() => {
    let muted: boolean = false;
    const storageData = localStorage.getItem(`${timerId}-muted`);
    if (storageData != null) {
      muted = JSON.parse(storageData);
    }
    updateMuted(muted);
  }, [timerId, updateMuted]);

  // volume 初期化
  useEffect(() => {
    let volume: number | "" = 100;
    const storageData = localStorage.getItem(`${timerId}-volume`);
    if (storageData != null) {
      volume = JSON.parse(storageData);
    }
    updateVolume(volume);
  }, [timerId, updateVolume]);

  // audio onEnded 処理
  useEffect(() => {
    if (audioElementRef.current == null) {
      return;
    }

    audioElementRef.current.addEventListener("ended", () => {
      onEnd?.();
    });
  }, [onEnd]);

  // setInterval 処理
  useEffect(() => {
    const intervalId = setInterval(() => {
      const now = DateTime.local();
      const day = now.day;
      const hour = now.hour;
      const minute = now.minute;

      // 鳴らすべき時間かどうかを判定
      const index = times.findIndex((time) => {
        return isTimeForPlay(time, day, hour, minute);
      });
      if (index === -1) {
        return;
      }

      // 以下、鳴らすべき時間にのみ行う処理
      setTimes([
        ...times.slice(0, index),
        { hour, minute, playedDate: day },
        ...times.slice(index + 1),
      ]);

      playAudio(audioElementRef.current, muted);
      notify(hour, minute);
      onPlay?.();
    }, 1000 * 5);

    return () => {
      clearInterval(intervalId);
    };
  }, [muted, onPlay, times]);

  return {
    times,
    muted,
    volume,
    updateTimes,
    updateMuted,
    updateVolume,
    audioElementRef,
  };
};
