import { Times } from "../types/timer";

export const sample1: Times = [
  { hour: 10, minute: 0 },
  { hour: 10, minute: 50 },
  { hour: 11, minute: 0 },
  { hour: 11, minute: 50 },
  { hour: 12, minute: 0 },
  { hour: 12, minute: 50 },
  { hour: 13, minute: 0 },
  { hour: 13, minute: 50 },
  { hour: 14, minute: 0 },
  { hour: 14, minute: 50 },
  { hour: 15, minute: 0 },
  { hour: 15, minute: 50 },
  { hour: 16, minute: 0 },
  { hour: 16, minute: 50 },
  { hour: 17, minute: 0 },
  { hour: 17, minute: 50 },
  { hour: 18, minute: 0 },
  { hour: 18, minute: 50 },
  { hour: 19, minute: 0 },
];
