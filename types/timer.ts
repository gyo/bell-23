export type Time = {
  hour: number;
  minute: number;
  playedDate?: number;
};

export type Times = Time[];
