import { NextPage } from "next";
import React from "react";
import { Timer } from "../components/Timer/";
import { refreshPlayedDates } from "../hooks/timer/utils";
import { sample1 } from "../samples/sample1";
import { sample2 } from "../samples/sample2";
import { sample3 } from "../samples/sample3";

const sample1Date = refreshPlayedDates(sample1);
const sample2Date = refreshPlayedDates(sample2);
const sample3Date = refreshPlayedDates(sample3);

const Page: NextPage = () => {
  React.useEffect(() => {
    window.addEventListener("load", () => {
      if (window.Notification == null) {
        return;
      }

      if (window.Notification.permission === "granted") {
        return;
      }

      window.Notification.requestPermission();
    });
  }, []);

  return (
    <>
      <div className="columns">
        <div className="column">
          <Timer
            defaultTimes={sample1Date}
            title="gakkou"
            timerId="0"
            audioSrc="/audio/Japanese_School_Bell02-01.mp3"
          />
        </div>
        <div className="column">
          <Timer
            defaultTimes={sample2Date}
            title="horagai"
            timerId="1"
            audioSrc="/audio/Horagai01-1.mp3"
          />
        </div>
        <div className="column">
          <Timer
            defaultTimes={[]}
            title="konbini"
            timerId="2"
            audioSrc="/audio/Doorbell-Melody01-1.mp3"
          />
        </div>
        <div className="column">
          <Timer
            defaultTimes={[]}
            title="poteto"
            timerId="3"
            audioSrc="/audio/Warning-Alarm02-1L.mp3"
          />
        </div>
        <div className="column">
          <Timer
            defaultTimes={sample3Date}
            title="hotaru"
            timerId="4"
            audioSrc="/audio/Auld_Lang_Syne_xf_h_Healing_Harp.mp3"
          />
        </div>
      </div>
    </>
  );
};

export default Page;
