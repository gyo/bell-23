import React from "react";

import Backdrop from "@material-ui/core/Backdrop";
import Paper from "@material-ui/core/Paper";
import Dialog, { DialogProps } from "@material-ui/core/Dialog";
import { withStyles } from "@material-ui/core/styles";

const MyBackdrop = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
  },
})(Backdrop);

const MyPaper = withStyles({
  root: {
    boxShadow: "none",
    border: "1px solid var(--color-foreground-3)",
    backgroundColor: "var(--color-background-1)",
  },
})(Paper);

export const MyDialog = withStyles({})((props: DialogProps) => (
  <Dialog BackdropComponent={MyBackdrop} PaperComponent={MyPaper} {...props} />
));
