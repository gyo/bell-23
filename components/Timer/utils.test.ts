import { DateTime } from "luxon";
import {
  convertTextWithColonToTimes,
  convertTextWithoutColonToTimes,
  convertTimesToText,
  detectTextFormat,
  sortTimes,
} from "./utils";

describe("detectTextFormat", () => {
  test("with colon", () => {
    expect(detectTextFormat("23:59\n01:01\n")).toBe("WITH_COLON");
  });

  test("without colon", () => {
    expect(detectTextFormat("1\n2\n")).toBe("WITHOUT_COLON");
  });
});

test("convertTimesToText", () => {
  expect(
    convertTimesToText([
      { hour: 1, minute: 1 },
      { hour: 23, minute: 59 },
    ])
  ).toBe("01:01\n23:59\n");
});

test("convertTextWithColonToTimes", () => {
  expect(convertTextWithColonToTimes("23:59\n01:01\nhello")).toEqual([
    { hour: 0, minute: 0 },
    { hour: 1, minute: 1 },
    { hour: 23, minute: 59 },
  ]);
});

test("convertTextWithoutColonToTimes", () => {
  const now = DateTime.local(1999, 12, 31, 23, 58, 7, 777);
  expect(convertTextWithoutColonToTimes("1\n2\n3\n", now)).toEqual([
    { hour: 0, minute: 1 },
    { hour: 0, minute: 4 },
    { hour: 23, minute: 59 },
  ]);
});

test("sortTimes", () => {
  expect(
    sortTimes([
      { hour: 3, minute: 1 },
      { hour: 2, minute: 2 },
      { hour: 2, minute: 1 },
      { hour: 1, minute: 2 },
      { hour: 1, minute: 1 },
    ])
  ).toStrictEqual([
    { hour: 1, minute: 1 },
    { hour: 1, minute: 2 },
    { hour: 2, minute: 1 },
    { hour: 2, minute: 2 },
    { hour: 3, minute: 1 },
  ]);
});
