import { DateTime } from "luxon";
import React, {
  ChangeEvent,
  FormEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";

import {
  Close,
  Delete,
  Edit,
  NotificationsActive,
  NotificationsOff,
} from "@material-ui/icons";

import { useTimer } from "../../hooks/timer";
import { Times } from "../../types/timer";
import { MyDialog } from "../MyDialog";
import {
  convertTextWithColonToTimes,
  convertTextWithoutColonToTimes,
  convertTimesToText,
  detectTextFormat,
} from "./utils";

type Props = {
  defaultTimes: Times;
  title: string;
  timerId: string;
  audioSrc: string;
};

export const Timer: React.FC<Props> = (props) => {
  const [opened, setOpened] = useState(false);
  const [editing, setEditing] = useState(false);

  const handleOpenDialog = useCallback(() => {
    setOpened(true);
  }, []);

  const handleCloseDialog = useCallback(() => {
    setOpened(false);
  }, []);

  const {
    times,
    muted,
    volume,
    updateTimes,
    updateMuted,
    updateVolume,
    audioElementRef,
  } = useTimer(
    props.timerId,
    props.defaultTimes,
    handleOpenDialog,
    handleCloseDialog
  );

  const [localText, setLocalText] = useState(convertTimesToText(times));

  const textareaRef = useRef<HTMLTextAreaElement>(null);

  const now = DateTime.local();
  const day = now.day;

  // times の初期化後に localText を初期化する
  useEffect(() => {
    setLocalText(convertTimesToText(times));
  }, [times]);

  // volume の初期化後に audioElement の volume を初期化する
  useEffect(() => {
    if (audioElementRef.current == null || volume === "") {
      return;
    }
    audioElementRef.current.volume = volume / 100;
  });

  // 編集開始時に textarea にフォーカスする
  useEffect(() => {
    textareaRef.current?.focus();
  }, [editing]);

  // escape キーで編集を終了するための keyup イベントを設定する
  useEffect(() => {
    if (editing) {
      const listener = (e: KeyboardEvent) => {
        if (e.code === "Escape") {
          setEditing(false);
        }
      };
      if (editing) {
        window.addEventListener("keyup", listener);
      }
      return () => {
        window.removeEventListener("keyup", listener);
      };
    }
  }, [editing]);

  const handleToggleMute = useCallback(() => {
    updateMuted(!muted);
  }, [updateMuted, muted]);

  const handleChangeVolume = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const value = e.target.value;

      // input[type="number"] が空の場合、input の値とデータのみ更新
      if (value === "") {
        updateVolume("");
        return;
      }

      // input の値が不正の場合、なにもしない
      let numberValue = parseInt(value, 10);
      if (numberValue < 0 || 100 < numberValue || isNaN(numberValue)) {
        return;
      }

      // input の値とデータを更新し、HTMLAudioElement の volume を設定
      updateVolume(numberValue);
      if (audioElementRef.current == null) {
        return;
      }
      audioElementRef.current.volume = numberValue / 100;
    },
    [updateVolume, audioElementRef]
  );

  const handleChangeText = useCallback(
    (e: ChangeEvent<HTMLTextAreaElement>) => {
      setLocalText(e.target.value);
    },
    []
  );

  const handleClickEditButton = useCallback(() => {
    setEditing(true);
  }, []);

  const handleSubmit = useCallback(
    (e: FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      const textFormat = detectTextFormat(localText);
      if (textFormat === "WITH_COLON") {
        const TimesFromText = convertTextWithColonToTimes(localText);
        updateTimes(TimesFromText);
      } else if (textFormat === "WITHOUT_COLON") {
        const TimesFromText = convertTextWithoutColonToTimes(
          localText,
          DateTime.local()
        );
        updateTimes(TimesFromText);
      }
      setEditing(false);
    },
    [localText, updateTimes]
  );

  const handleClearTimes = useCallback(() => {
    updateTimes([]);
  }, [updateTimes]);

  const handlePushTime = useCallback(
    (minutes: number) => () => {
      const newDateTime = DateTime.local().plus({ minutes });
      updateTimes([
        ...times,
        {
          hour: newDateTime.hour,
          minute: newDateTime.minute,
        },
      ]);
    },
    [times, updateTimes]
  );

  const handlePlayAudio = useCallback(() => {
    if (audioElementRef.current == null) {
      alert("audioElementRef is not found!");
      return;
    }
    audioElementRef.current.currentTime = 0;
    audioElementRef.current.play();
  }, [audioElementRef]);

  const handlePauseAudio = useCallback(() => {
    if (audioElementRef.current == null) {
      alert("audioElementRef is not found!");
      return;
    }
    audioElementRef.current.pause();
    audioElementRef.current.currentTime = 0;
  }, [audioElementRef]);

  const handleClickDialogPauseButton = useCallback(() => {
    handleCloseDialog();
    handlePauseAudio();
  }, [handleCloseDialog, handlePauseAudio]);

  return (
    <section className="timer">
      <div className="timer__header">
        <div className="timer-header">
          {muted ? (
            <button type="button" onClick={handleToggleMute}>
              <span className="muted">
                <NotificationsOff />
              </span>
            </button>
          ) : (
            <button type="button" onClick={handleToggleMute}>
              <NotificationsActive />
            </button>
          )}
          <h1 className="timer-header__title">{props.title}</h1>
        </div>
      </div>

      <div className="timer__volume">
        Volume:
        <input
          type="number"
          min="0"
          max="100"
          value={muted ? 0 : volume}
          onChange={handleChangeVolume}
          disabled={muted}
        />
      </div>

      {editing && (
        <form className="timer__form" onSubmit={handleSubmit}>
          <div className="timer-textarea-wrapper">
            <textarea
              className="timer-textarea"
              name="text"
              ref={textareaRef}
              value={localText}
              onChange={handleChangeText}
            />
          </div>

          <div className="timer-form-button-wrapper">
            <button className="timer-button">反映</button>
          </div>
        </form>
      )}

      {!editing && (
        <>
          <div className="timer__list-wrapper">
            <ul className="timer__list">
              {times.map((time, index) => (
                <li key={index}>
                  {day === time.playedDate ? "*" : ""}
                  {`${time.hour}`.padStart(2, "0")}:
                  {`${time.minute}`.padStart(2, "0")}
                </li>
              ))}
            </ul>

            <div className="timer__list-actions">
              <button type="button" onClick={handleClickEditButton}>
                <Edit />
              </button>
              <button type="button" onClick={handleClearTimes}>
                <Delete />
              </button>
            </div>
          </div>

          <div className="timer__button-wrapper">
            <button
              className="timer-button"
              type="button"
              onClick={handleClickEditButton}
            >
              編集
            </button>
          </div>
        </>
      )}

      <div className="timer__button-wrapper">
        <div className="timer__button-separator">
          <button
            className="timer-button"
            type="button"
            onClick={handlePushTime(5)}
          >
            5分後
          </button>
          <button
            className="timer-button"
            type="button"
            onClick={handlePushTime(30)}
          >
            30分後
          </button>
        </div>
      </div>

      <div className="timer__button-wrapper">
        <div className="timer__button-separator">
          <button
            className="timer-button"
            type="button"
            onClick={handlePushTime(15)}
          >
            15分後
          </button>
          <button
            className="timer-button"
            type="button"
            onClick={handlePushTime(40)}
          >
            40分後
          </button>
        </div>
      </div>

      <div className="timer__button-wrapper">
        <button
          className="timer-button"
          type="button"
          onClick={handleClearTimes}
        >
          すべて削除
        </button>
      </div>

      <div className="timer__button-wrapper">
        <button
          className="timer-button"
          type="button"
          onClick={handlePlayAudio}
        >
          テスト
        </button>
      </div>

      <div className="timer__button-wrapper">
        <button
          className="timer-button"
          type="button"
          onClick={handlePauseAudio}
        >
          停止
        </button>
      </div>

      <audio ref={audioElementRef} src={props.audioSrc}></audio>

      <MyDialog open={opened} onClose={handleCloseDialog}>
        <div className="dialog">
          <div className="dialog__close-button">
            <button type="button" onClick={handleCloseDialog}>
              <Close />
            </button>
          </div>
          <button
            className="dialog__button"
            type="button"
            onClick={handleClickDialogPauseButton}
          >
            停止
          </button>
        </div>
      </MyDialog>
    </section>
  );
};
