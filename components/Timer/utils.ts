import { DateTime } from "luxon";
import { Times } from "../../types/timer";

export const sortTimes = (times: Times) => {
  return times.sort((a, b) => {
    if (a.hour < b.hour) {
      return -1;
    } else if (a.hour > b.hour) {
      return 1;
    } else if (a.minute < b.minute) {
      return -1;
    } else if (a.minute > b.minute) {
      return 1;
    } else {
      return 1;
    }
  });
};

export const convertTimesToText = (times: Times) => {
  return sortTimes(times).reduce((acc, current) => {
    return (
      acc +
      `${current.hour}`.padStart(2, "0") +
      ":" +
      `${current.minute}`.padStart(2, "0") +
      "\n"
    );
  }, "");
};

export const convertTextWithColonToTimes = (text: string) => {
  const times = text
    .trim()
    .split("\n")
    .filter((item) => item !== "")
    .map((item) => {
      const [hour, minute] = item.trim().split(":");
      const intHour = parseInt(hour, 10);
      const intMinute = parseInt(minute, 10);
      return {
        hour: isNaN(intHour) ? 0 : intHour,
        minute: isNaN(intMinute) ? 0 : intMinute,
      };
    });
  return sortTimes(times);
};

export const convertTextWithoutColonToTimes = (text: string, now: DateTime) => {
  const times = text
    .trim()
    .split("\n")
    .filter((item) => item !== "")
    .map((item) => {
      const intItem = parseInt(item);
      const result = isNaN(intItem) ? 0 : intItem;
      return result;
    })
    .reduce<number[]>((acc, current) => {
      if (acc.length === 0) {
        acc.push(current);
      } else {
        const sum = acc[acc.length - 1] + current;
        acc.push(sum);
      }
      return acc;
    }, [])
    .map((item) => {
      const nextDateTime = now.plus({ minutes: item });
      return {
        hour: nextDateTime.hour,
        minute: nextDateTime.minute,
      };
    });
  return sortTimes(times);
};

export const detectTextFormat = (text: string) => {
  const firstLine = text.trim().split("\n")[0];
  if (firstLine.includes(":")) {
    return "WITH_COLON";
  } else {
    return "WITHOUT_COLON";
  }
};
